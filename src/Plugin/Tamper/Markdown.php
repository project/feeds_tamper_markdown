<?php

namespace Drupal\feeds_tamper_markdown\Plugin\Tamper;

use Drupal\Core\Form\FormStateInterface;
use Drupal\tamper\Exception\TamperException;
use Drupal\tamper\TamperableItemInterface;
use Drupal\tamper\TamperBase;
use League\CommonMark\Environment\Environment;
use League\CommonMark\Extension\CommonMark\CommonMarkCoreExtension;
use League\CommonMark\MarkdownConverter;

/**
 * Plugin implementation for Markdown.
 *
 * @Tamper(
 *   id = "markdown",
 *   label = @Translation("Converts Markdown to HTML"),
 *   description = @Translation("Converts Markdown to HTML."),
 *   category = "Text"
 * )
 */
class Markdown extends TamperBase {

  const ALLOW_UNSAFE_LINKS = 1;

  const HTML_INPUT = 'default';

  const ENABLE_EM = TRUE;

  const ENABLE_STRONG = TRUE;

  const USE_ASTERISK = TRUE;

  const USE_UNDERSCORE = TRUE;

  const UNORDERED_LIST_MARKERS = ['-', '*', '+'];

  const EXTENSIONS = [
    'DescriptionListExtension' => [
      'description' => 'Description List Extension',
      'info' => 'https://commonmark.thephpleague.com/2.4/extensions/description-lists/',
      'namespace' => '\League\CommonMark\Extension\DescriptionList\DescriptionListExtension',
    ],
    'AttributesExtension' => [
      'description' => 'Attributes',
      'info' => 'https://commonmark.thephpleague.com/2.4/extensions/attributes/',
      'namespace' => 'League\CommonMark\Extension\Attributes\AttributesExtension',
    ],
    'GithubFlavoredMarkdownExtension' => [
      'description' => 'GitHub-Flavored Markdown',
      'info' => 'https://commonmark.thephpleague.com/2.4/extensions/github-flavored-markdown/',
      'namespace' => 'League\CommonMark\Extension\GithubFlavoredMarkdownExtension',
    ],
    'FootnoteExtension' => [
      'description' => 'Footnotes',
      'info' => 'https://commonmark.thephpleague.com/2.4/extensions/footnotes/',
      'namespace' => 'League\CommonMark\Extension\FootnoteExtension',
    ],
    'InlinesOnlyExtension' => [
      'description' => 'Inlines Only Extension',
      'info' => 'https://commonmark.thephpleague.com/2.4/extensions/inlines-only/',
      'namespace' => 'League\CommonMark\Extension\InlinesOnly\InlinesOnlyExtension',
    ],
      'SmartPunctExtension' => [
      'description' => 'Smart Punctuation Extension',
      'info' => 'https://commonmark.thephpleague.com/2.4/extensions/smart-punctuation/',
      'namespace' => 'League\CommonMark\Extension\SmartPunct\SmartPunctExtension',
    ],
  ];

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $config = parent::defaultConfiguration();
    $config['allow_unsafe_links'] = self::ALLOW_UNSAFE_LINKS;
    $config['html_input'] = self::HTML_INPUT;
    $config['enable_em'] = self::ENABLE_EM;
    $config['enable_strong'] = self::ENABLE_STRONG;
    $config['use_asterisk'] = self::USE_ASTERISK;
    $config['use_underscore'] = self::USE_UNDERSCORE;
    $config['unordered_list_markers'] = self::UNORDERED_LIST_MARKERS;
    return $config;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['config'] = [
      '#type' => 'details',
      '#title' => $this->t('Configuration'),
      '#open' => FALSE,
    ];

    $form['config']['html_input'] = [
      '#type' => 'select',
      '#title' => $this->t('How to handle HTML input.'),
      '#options' => [
        'allow' => $this->t('Allow'),
        'escape' => $this->t('Escape'),
        'strip' => $this->t('Strip'),
      ],
      '#default_value' => $this->getSetting('html_input'),
      '#description' => $this->t('If you’re developing an application which renders user-provided Markdown from potentially untrusted users, you are strongly encouraged to set the html_input option in your configuration to either escape or strip.'),
    ];

    $form['config']['allow_unsafe_links'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Remove risky link and image URLs'),
      '#default_value' => $this->getSetting('allow_unsafe_links'),
      '#description' => $this->t('Whether unsafe links are permitted.'),
    ];

    $form['config']['max_nesting_level'] = [
      '#type' => 'number',
      '#title' => $this->t('Nesting Level'),
      '#size' => 6,
      '#description' => $this->t('No maximum nesting level is enforced
      by default. <br> If you need to parse untrusted input,
      consider setting a reasonable max_nesting_level (perhaps 10-50)
      depending on your needs.<br>
      Once this nesting level is hit, any subsequent Markdown will be rendered
      as plain text.'),
      '#default_value' => $this->getSetting('max_nesting_level'),
    ];
    $form['config']['enable_em'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('&lt;em&gt; parsing.'),
      '#default_value' => $this->getSetting('enable_em'),
      '#description' => $this->t('Un-check to disable &lt;em&gt; parsing.'),
    ];
    $form['config']['enable_strong'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('&lt;strong&gt; parsing.'),
      '#default_value' => $this->getSetting('enable_strong'),
      '#description' => $this->t('Un-check to disable &lt;strong&gt; parsing.'),
    ];
    $form['config']['use_asterisk'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Asterisk * parsing.'),
      '#default_value' => $this->getSetting('use_asterisk'),
      '#description' => $this->t('Un-check to disable * parsing.'),
    ];
    $form['config']['use_underscore'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('_ parsing.'),
      '#default_value' => $this->getSetting('use_underscore'),
      '#description' => $this->t('Un-check to disable parsing of _ for emphasis.'),
    ];
    $form['extensions'] = [
      '#type' => 'details',
      '#title' => $this->t('Extensions'),
      '#open' => FALSE,
    ];
    // build extensions options.
    foreach (self::EXTENSIONS as $extensions_names => $extensions_values) {
      $extensions_options[$extensions_names] = $extensions_values['description'];
    }
    $form['extensions']['extension'] = [
      '#type' => 'checkboxes',
      '#options' => $extensions_options,
      '#default_value' => $this->getSetting('extensions'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $html_input = $form_state->getValue([
      'config',
      'html_input',
    ]);
    $allow_unsafe_links = $form_state->getValue([
      'config',
      "allow_unsafe_links",
    ]);
    $allow_unsafe_links = ($allow_unsafe_links == 1) ? TRUE : FALSE;
    $max_nesting_level = $form_state->getValue([
      'config',
      "max_nesting_level",
    ]);
    $enable_em = $form_state->getValue([
      'config',
      "enable_em",
    ]);
    $enable_em = (empty($enable_em)) ? FALSE : TRUE;
    $enable_strong = $form_state->getValue([
      'config',
      "enable_strong",
    ]);
    $enable_strong = (empty($enable_strong)) ? FALSE : TRUE;
    $use_asterisk = $form_state->getValue([
      'config',
      "use_asterisk",
    ]);
    $use_asterisk = (empty($use_asterisk)) ? FALSE : TRUE;
    $use_underscore = $form_state->getValue([
      'config',
      "use_underscore",
    ]);
    $use_underscore = (empty($use_underscore)) ? FALSE : TRUE;
    $extensions = $form_state->getValue([
      'extensions',
      'extension',
    ]);

    $extensions = array_filter($extensions);
    $this->setConfiguration([
      'html_input' => $html_input,
      'allow_unsafe_links' => $allow_unsafe_links,
      'max_nesting_level' => $max_nesting_level,
      "enable_em" => $enable_em,
      "enable_strong" => $enable_strong,
      "use_asterisk" => $use_asterisk,
      "use_underscore" => $use_underscore,
      'extensions' => $extensions,
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function tamper($data, TamperableItemInterface $item = NULL) {
    if (!is_string($data)) {
      throw new TamperException('Input should be a string.');
    }
    $config = [];
    $html_input_values = $this->getSetting('html_input');
    $config['commonmark'] = [
      'enable_em' => $this->getSetting('enable_em'),
      'enable_strong' => $this->getSetting('enable_strong'),
      'use_asterisk' => $this->getSetting('use_asterisk'),
      'use_underscore' => $this->getSetting('use_underscore'),
      'unordered_list_markers' => $this->getSetting('unordered_list_markers'),
    ];
    $config['html_input'] = $html_input_values;
    $config['allow_unsafe_links'] = $this->getSetting('allow_unsafe_links');
    $config['max_nesting_level'] = (int) $this->getSetting('max_nesting_level');
    $environment = new Environment($config);
    $environment->addExtension(new CommonMarkCoreExtension());
    $extension_array = $this->getSetting('extensions');
    $available_extensions = self::EXTENSIONS;
    if (!empty($extension_array)) {
      foreach (array_keys($extension_array) as $extension_name) {
        $ext = $available_extensions[$extension_name]['namespace'];
        // Instantiate selected extensions.
        $environment->addExtension(new $ext());
      }
    }
    $converter = new MarkdownConverter($environment);
    return $converter->convert($data);
  }
}
